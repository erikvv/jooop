package main

import (
	"bytes"
	"context"
	"log"
	"os"
	"runtime"
	"runtime/pprof"
	"time"

	"storj.io/uplink"
)

func main() {
	err := doIt()
	if err != nil {
		println(err.Error())
	}
	//println("Second")
	//err = doIt()
	//if err != nil {
	//	println(err.Error())
	//}
	runtime.GC()

	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	log.Println("MEMORY STATS")
	println("HeapSys: ", m.HeapSys)
	println("HeapAlloc: ", m.HeapAlloc)
	println("HeapIdle: ", m.HeapIdle)
	println("HeapReleased: ", m.HeapReleased)
	println("HeapInuse: ", m.HeapInuse)

	//println("done with uplking, writing heap profile")

	file, err := os.Create("heap.out")
	if err != nil {
		println(err.Error())
		return
	}
	err = pprof.Lookup("heap").WriteTo(file, 0)
	if err != nil {
		println(err.Error())
		return
	}

	//file, err := os.Create("heap.out")
	//err = pprof.WriteHeapProfile(file)
	//if err != nil {
	//	println(err.Error())
	//	return
	//}
	err = file.Close()
	if err != nil {
		println(err.Error())
		return
	}

	println("done, sleeping")
	time.Sleep(50 * time.Minute)
}

func doIt() (err error) {
	parentCtx := context.Background()
	ctx, cancel := context.WithCancel(parentCtx)
	defer cancel()

	nProjects := 10
	nObjects := 1
	nChunks := 100_000
	chunkSize := 1000

	for i := 0; i < nProjects; i++ {
		access, err := uplink.ParseAccess("")
		if err != nil {
			return err
		}

		config := uplink.Config{}

		project, err := config.OpenProject(ctx, access)
		if err != nil {
			return err
		}
		defer func(project *uplink.Project) {
			err = project.Close()
		}(project)

		for j := 0; j < nObjects; j++ {
			upload, err := project.UploadObject(ctx, "bucket-in-c", "object88", nil)
			if err != nil {
				return err
			}

			data := bytes.Repeat([]byte{0xab}, chunkSize)
			for k := 0; k < nChunks; k++ {
				_, err = upload.Write(data)
				if err != nil {
					return err
				}
			}

			err = upload.Commit()
			if err != nil {
				return err
			}
		}
	}

	return nil
}
